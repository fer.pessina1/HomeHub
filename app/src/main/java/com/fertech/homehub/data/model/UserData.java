package com.fertech.homehub.data.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fer on 21/10/2017.
 * UserData model
 */

public class UserData {
    private List<Light> lights;
    private List<WindowBlind> blinds;

    public UserData() {
        this.lights = new ArrayList<>();
        this.blinds = new ArrayList<>();
    }

    public UserData(List<Light> lights, List<WindowBlind> blinds) {
        this.lights = lights;
        this.blinds = blinds;
    }

    public List<Light> getLights() {
        return lights;
    }

    public void setLights(List<Light> lights) {
        this.lights = lights;
    }

    public List<WindowBlind> getBlinds() {
        return blinds;
    }

    public void setBlinds(List<WindowBlind> blinds) {
        this.blinds = blinds;
    }
}
