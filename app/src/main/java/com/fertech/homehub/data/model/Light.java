package com.fertech.homehub.data.model;

/**
 * Created by Fer on 21/10/2017.
 *
 * Non dimmering Light model
 */

public class Light {
    private String name;
    private boolean state;
    private boolean hasClapper;

    public Light() {
    }

    public Light(String name, boolean state, boolean hasClapper) {
        this.name = name;
        this.state = state;
        this.hasClapper = hasClapper;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public boolean getHasClapper() {
        return hasClapper;
    }

    public void setHasClapper(boolean hasClapper) {
        this.hasClapper = hasClapper;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!Light.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        final Light other = (Light) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if (this.state != other.state) {
            return false;
        }
        if (this.hasClapper != other.hasClapper) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 53 * hash + (this.state ? 13 : 0);
        hash = 53 * hash + (this.hasClapper ? 63 : 5);
        return hash;
    }
}
