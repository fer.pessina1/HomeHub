package com.fertech.homehub.data.mqtt;

/**
 * Created by Fer on 27/10/2017.
 *
 */

public interface ConnectionStatusListener {

    public void OnConnectSuccess();

    public void OnConnectionError();

    public void OnDisconnect();

}