package com.fertech.homehub.data.model;

/**
 * Created by Fer on 21/10/2017.
 * Window blind model
 */

public class WindowBlind {
    private String name;
    private int currentPosition;
    private int targetPosition;
    private int maxPosition;

    public WindowBlind() {
    }

    public WindowBlind(WindowBlind blind) {
        this.name = blind.getName();
        this.currentPosition = blind.getCurrentPosition();
        this.targetPosition = blind.getTargetPosition();
        this.maxPosition = blind.getMaxPosition();
    }

    public WindowBlind(String name, int currentPosition, int targetPosition, int maxPosition) {
        this.name = name;
        this.currentPosition = currentPosition;
        this.targetPosition = targetPosition;
        this.maxPosition = maxPosition;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    public int getTargetPosition() {
        return targetPosition;
    }

    public void setTargetPosition(int targetPosition) {
        this.targetPosition = targetPosition;
    }

    public int getMaxPosition() {
        return maxPosition;
    }

    public void setMaxPosition(int maxPosition) {
        this.maxPosition = maxPosition;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!WindowBlind.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        final WindowBlind other = (WindowBlind) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if (this.currentPosition != other.currentPosition) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 53 * hash + this.currentPosition;
        return hash;
    }
}

