package com.fertech.homehub;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.fertech.homehub.data.model.UserData;
import com.fertech.homehub.data.model.WindowBlind;
import com.fertech.homehub.data.mqtt.MQTTClient;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RoomControl extends AppCompatActivity{
    private String TAG = "RoomControl";
    private LinearLayout blindsLayout;
    private Toolbar toolbar;
    private UserData data;
    private MQTTClient mqttClient;
    private Map<String, View> blindControlsMap = new HashMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_control);
        toolbar = findViewById(R.id.toolbar_room_control);
        setSupportActionBar(toolbar);

        blindsLayout = findViewById(R.id.windowBlindsContainer);

        data = new UserData();

        mqttClient = MQTTClient.getInstance(this.getApplicationContext());
        mqttClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {

            }

            @Override
            public void connectionLost(Throwable cause) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                String payload = new String(message.getPayload());
                Log.i(TAG, "Received message: "+topic+" "+payload);
                String[] keys = topic.split("/");
                if (keys[0].equals("devices")){
                    if (keys[1].equals("blinds")) {
                        String blindName = keys[2];
                        String param = keys[3];
                        int value = Integer.valueOf(payload);
                        Boolean found = false;
                        for (WindowBlind blind : data.getBlinds()){
                            if (blind.getName().equals(blindName)) {
                                found = true;
                                switch (param){
                                    case "currentPosition":
                                        blind.setCurrentPosition(value);
                                        break;
                                    case "targetPosition":
                                        blind.setTargetPosition(value);
                                        break;
                                    case "maxPosition":
                                        blind.setMaxPosition(value);
                                        break;
                                }
                            }
                        }
                        if (!found) {
                            WindowBlind blind = new WindowBlind(blindName, 0, 0, 0);
                            switch (param){
                                case "currentPosition":
                                    blind.setCurrentPosition(value);
                                    break;
                                case "targetPosition":
                                    blind.setTargetPosition(value);
                                    break;
                                case "maxPosition":
                                    blind.setMaxPosition(value);
                                    break;
                            }
                            data.getBlinds().add(blind);
                        }
                        updateBlinds(data.getBlinds());
                    }
                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });

    }

    // Menu icons are inflated just as they were with actionbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.room_control_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.reconnect:
                // User chose the "Settings" item, show the app settings UI...
                mqttClient.connect();

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private void updateBlinds(List<WindowBlind> blinds) {
        for (WindowBlind blind : blinds){
            String name = blind.getName();
            View blindControl = blindControlsMap.get(name);
            if(blindControl != null){
                SeekBar targetSeekBar = blindControl.findViewById(R.id.targetSeekBar);
                SeekBar positionSeekBar = blindControl.findViewById(R.id.positionSeekBar);
                targetSeekBar.setMax(blind.getMaxPosition());
                targetSeekBar.setProgress(blind.getTargetPosition());
                positionSeekBar.setMax(blind.getMaxPosition());
                positionSeekBar.setProgress(blind.getCurrentPosition());

            } else {
                LayoutInflater inflater = (LayoutInflater)getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                blindControl = inflater.inflate(R.layout.window_blind_control, blindsLayout);
                final SeekBar targetSeekBar = blindControl.findViewById(R.id.targetSeekBar);
                final SeekBar positionSeekBar = blindControl.findViewById(R.id.positionSeekBar);
                targetSeekBar.setMax(blind.getMaxPosition());
                targetSeekBar.setProgress(blind.getTargetPosition());
                positionSeekBar.setMax(blind.getMaxPosition());
                positionSeekBar.setProgress(blind.getCurrentPosition());
                targetSeekBar.setTag(R.id.blind, blind.getName());
                targetSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        String blindName = (String) seekBar.getTag(R.id.blind);
                        int i = seekBar.getProgress();
                        String topic = "devices/blinds/"+blindName+"/targetPosition";
                        byte[] payload = Integer.toString(i).getBytes();
                        mqttClient.publish(topic, payload);
                    }
                });
                ImageButton openButton = blindControl.findViewById(R.id.buttonOpen);
                ImageButton closeButton = blindControl.findViewById(R.id.buttonClose);
                TextView blindNameTextView = blindControl.findViewById(R.id.blindNameTextView);
                openButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        targetSeekBar.setProgress(targetSeekBar.getMax());
                        String blindName = (String) targetSeekBar.getTag(R.id.blind);
                        String topic = "devices/blinds/"+blindName+"/targetPosition";
                        byte[] payload = Integer.toString(targetSeekBar.getMax()).getBytes();
                        mqttClient.publish(topic, payload);
                    }
                });
                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        targetSeekBar.setProgress(0);
                        String blindName = (String) targetSeekBar.getTag(R.id.blind);
                        String topic = "devices/blinds/"+blindName+"/targetPosition";
                        byte[] payload = Integer.toString(0).getBytes();
                        mqttClient.publish(topic, payload);
                    }
                });
                String blindName = blind.getName().split("\\.")[1];
                blindNameTextView.setText(blindName);
                blindControlsMap.put(blind.getName(), blindControl);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

}
